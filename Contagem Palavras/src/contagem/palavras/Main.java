//--Função da classe: 
/*Nesta class é implementada o teste para o trabalho
, são utilizados try's and catch's para que as exceções necessárias sejam tratadas
, aqui também é realizado o loop para verificar quantos arquivos serão comparados 
e o nomes dos respectivos, logo após os arquivos textos serão processados,
 e então é gerado um arquivo resultado.out 
contendo as palavras comparadas em ordem alfabética juntamente com seu contador ao lado. */
package contagem.palavras;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Quantos arquivos deseja comparar?");
            int quantidadeArquivos = input.nextInt();

            ContagemPalavraListaDuplamenteEncadeada listaDePalavras = new ContagemPalavraListaDuplamenteEncadeada(quantidadeArquivos);

            for (int i = 0; i < quantidadeArquivos; i++) {
                System.out.println("Digite o nome" + (i + 1) + "° do arquivo:");
                String nomeDosArquivo = input.next();

                for (int indiceDoArquivo = 0; indiceDoArquivo < quantidadeArquivos; indiceDoArquivo++) {
                    processarTexto(nomeDosArquivo, listaDePalavras, indiceDoArquivo);
                }
            }

            /*gera o arquivo resultado.out a partir da lista duplamente encadeada listaDePalavras*/
            gravaArquivo(listaDePalavras);
        } catch (InputMismatchException campoInvalido) {
            System.out.println("Tipo de entrada invalida");
        } catch (FileNotFoundException fi) {
            /*esta excecao e lançada pelo metodo processarTexto() quando o arquivo nao existe*/
            System.out.println("Entrada inválida");
        }

    }

    /*este metodo processa um arquivo, adicionando as entradas na lista encadeada
    e lanca excecao FileNotFoundException caso o arquivo nao exista*/
    public static void processarTexto(String nome, ContagemPalavraListaDuplamenteEncadeada lista, int indexDoTexto) throws FileNotFoundException {

        try {
            FileReader arq = new FileReader(nome);
            Scanner scanner = new Scanner(arq);

            while (scanner.hasNext()) {
                lista.insercaoDePalavra(removerAcentuacao(scanner.next().toLowerCase()), indexDoTexto);
            }

        } catch (InputMismatchException campoInvalido) {
            System.out.println("Tipo de entrada inválida");
        }

    }

    /*Metodo para tratar acentos especiais que nao devem ser considerados como palavra*/
    private static String removerAcentuacao(String palavra) {
        return palavra.replaceAll("\\?", "")
                .replaceAll("\\.", "")
                .replaceAll(",", "")
                .replaceAll("!", "")
                .replaceAll("\\+", "")
                .replaceAll("%", "")
                .replaceAll("\\*", "");
    }

    /*Este metodo percorre a lista e retorna um arquivo resultado.out contendo
    as palavras em ordem alfabética juntamente com seu contador*/
    public static void gravaArquivo(ContagemPalavraListaDuplamenteEncadeada lista) {

        try (FileWriter arquivo = new FileWriter("resultado.out")) {
            PrintWriter gerarArq = new PrintWriter(arquivo);

            No noAtual = lista.getCabeca();
            while (noAtual != null) {
                gerarArq.printf("%s", noAtual.palavra);

                for (int i = 0; i < noAtual.contadorNoTexto.length; i++) {
                    gerarArq.printf(" %d", noAtual.contadorNoTexto[i]);
                }
                gerarArq.println();
                noAtual = noAtual.getProx();

            }
        } catch (IOException e) {
            System.err.println("erro ao gerar o resultado.out");
        }
    }
}
