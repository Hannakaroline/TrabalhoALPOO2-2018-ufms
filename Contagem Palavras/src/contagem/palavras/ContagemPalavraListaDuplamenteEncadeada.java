//--Função da classe: 
/*Nesta classe foi implementado uma lista duplamente encadeada:
Sua função é armazenar de forma ordenada as palavras recebida dos textos,
nela é utilizada o método: insercaoDePalavras() 
O método de insercaoDePalavras() percorre cria uma lista ordenada, além de ordena-la 
o método também verifica se a palavra já existe ao fazer a comparação de palavras,
para que o nó desta lista não possua palavras repetidas.
 */
package contagem.palavras;

public class ContagemPalavraListaDuplamenteEncadeada {

    private No cabeca;
    private int quantidadeDeTextos;

    public ContagemPalavraListaDuplamenteEncadeada(int quantidadeDeTextos) {
        this.quantidadeDeTextos = quantidadeDeTextos;
    }

    public No getCabeca() {
        return cabeca;
    }

    public void insercaoDePalavra(String novaPalavra, int indexDoTexto) {
        No proximoNo, noAux;

        if (cabeca == null) {
            No novo = new No(novaPalavra, quantidadeDeTextos);
            novo.contadorNoTexto[indexDoTexto]++;
            cabeca = novo;

        } else {
            proximoNo = cabeca;
            noAux = cabeca;
            while (proximoNo != null) {
                noAux = proximoNo;
                if (novaPalavra.compareTo(proximoNo.palavra) <= 0) {
                    break;
                }
                proximoNo = proximoNo.getProx();
            }

            int comparacaoDaChaveComNoAux = novaPalavra.compareTo(noAux.palavra);

            if (comparacaoDaChaveComNoAux == 0) {
                noAux.contadorNoTexto[indexDoTexto]++;
            } else if (comparacaoDaChaveComNoAux < 0) {
                No novoNo = new No(novaPalavra, quantidadeDeTextos);
                novoNo.contadorNoTexto[indexDoTexto]++;
                if (noAux == cabeca) {
                    noAux.setAnt(novoNo);
                    novoNo.setProx(cabeca);
                    cabeca = novoNo;
                } else {
                    novoNo.setProx(noAux);
                    novoNo.setAnt(noAux.getAnt());
                    noAux.getAnt().setProx(novoNo);
                    noAux.setAnt(novoNo);
                }
            } else {
                No novoNo = new No(novaPalavra, quantidadeDeTextos);
                novoNo.contadorNoTexto[indexDoTexto]++;
                noAux.setProx(novoNo);
                novoNo.setAnt(noAux);
            }

        }

    }
}
