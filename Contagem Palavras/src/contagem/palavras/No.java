//--Função da classe: 
/*Nesta classe foi implementada um No para ser utilizado na lista duplamente encadeada
da classe "ContagemPalavraListaDuplamenteEncadeada"
sua função é que para cada número de arquivos existente, seja criado um vetor
que possuirá um contador, na qual o mesmo será utilizado para contar o número de vezes 
que a palavra foi encontrado para cada texto.
São utilizado métodos Getters and Setters dos atributos: 
String palavra, que seria literalmente a palavra de cada texto,
um vetor de contador, e Nós auxiliares(anterior e proximo)*/
package contagem.palavras;

public class No {

    String palavra;
    int[] contadorNoTexto;
    No proximo;
    No anterior;

    public No() {
    }

    public No(String palavra, int quantidadeDeTextos) {
        this.palavra = palavra;
        contadorNoTexto = new int[quantidadeDeTextos];
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public No getProx() {
        return proximo;
    }

    public void setProx(No prox) {
        this.proximo = prox;
    }

    public No getAnt() {
        return anterior;
    }

    public void setAnt(No ant) {
        this.anterior = ant;
    }

}
